var AppDispatcher = require('../dispatchers/AppDispatcher');
var Constants = require('../constants/AppConstants');

module.exports = {

  refresh: function(){

    AppDispatcher.handleViewAction({
      type: Constants.ActionTypes.REFRESH_GAME
    });
  },
  clickOnField: function(x,y){
    AppDispatcher.handleViewAction({
      type: Constants.ActionTypes.CHOSE_CELL,
      data: {x: x, y: y}
    });
  },
  clearList: function() {
    console.warn('clearList action not yet implemented...');
  },

  completeTask: function(task) {
    console.warn('completeTask action not yet implemented...');
  }

};
