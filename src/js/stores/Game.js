var AppDispatcher = require('../dispatchers/AppDispatcher');
var Constants = require('../constants/AppConstants');
var BaseStore = require('./BaseStore');
var assign = require('object-assign');

var fields = [],
  minesCount = Constants.game.minesCount,
  xSize = Constants.game.sizeOfGame.x,
  ySize = Constants.game.sizeOfGame.y;


function checkBombNext(coords){
  var digit = 0;
  //console.log(coords, 'coords');
  //console.log(fields, 'fields');

  var digitX = parseInt(coords.x, 10),
    digitY = parseInt(coords.y, 10);

  //try{

    //if (fields[coords.x][coords.y+1] === Constants.game.cellStates.mine){
    //  digit++;
    //}

    if (fields[coords.x][coords.y-1] === Constants.game.cellStates.mine){
      digit++;
    }


    console.log();

    if (digitX+1 <= 10 && fields[getStringAddres(digitX+1)][coords.y] === Constants.game.cellStates.mine){
      digit++;
    }


    if (digitX-1 > 0 && fields[getStringAddres(digitX-1)][coords.y] === Constants.game.cellStates.mine){
      digit++;
    }

    fields[coords.x][coords.y] = (digit != 0) ? digit : Constants.game.cellStates.openAndNotMIne;

  //}catch(e){
  //  console.log(fields, 'fields');
  //
  //  //console.log(fields[getStringAddres(digitX+1)], '(fields[getStringAddres(digitX+1)]');
  //  //console.log(fields[getStringAddres(digitX-1)], 'fields[getStringAddres(digitX-1)');
  //
  //
  //
  //
  //  console.log(e.message);
  //}


  console.log(fields[coords.x][coords.y], 'digit');

  Game.emitChange();
  return digit;
}

function getStringAddres(data){
  return data.toString();
}

var Game = assign({}, BaseStore, {
  createFields: function(){
    var xCellCount = xSize,
      yCellCount = ySize;
    for (var x = xCellCount; x>0; --x){
      fields[x] = [];
      for (var y = yCellCount; y>0; --y){
        fields[x].push(this.getField())
      }

    }

    return fields;
  },
  getGame: function(){
    return fields;
  },
  getField: function() {

    if (minesCount !== 0 &&  Math.floor((Math.random() * 5) + 1) ===Math.floor((Math.random() * 5))){
     return 'm'
    }

    return 0;
  },
  // register store with dispatcher, allowing actions to flow through
  dispatcherIndex: AppDispatcher.register(function(payload) {
    var action = payload.action;

    switch(action.type) {
      case Constants.ActionTypes.REFRESH_GAME:
        alert('game refreshed'); //TODO it's method will contain code who refresh game
        break;
      case Constants.ActionTypes.CHOSE_CELL:
        checkBombNext(action.data);
        break;

      // add more cases for other actionTypes...
    }
  })

});

module.exports = Game;
