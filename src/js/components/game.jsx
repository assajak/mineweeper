var React = require('react');
var GameStore = require('./../stores/Game');
var cx = require('react/lib/cx');
var Constants = require('../constants/AppConstants');
var ActionCreator = require('../actions/game');


var game = React.createClass({
  getInitialState() {
    return {};
  },
  _onChange: function(){
    this.forceUpdate();
  },
  componentDidMount() {
    GameStore.addChangeListener(this._onChange);
  },
  renderRows: function(field){
    var rows = [];
    for (var id in field){
      rows.push(<div>{this.renderCells(field[id], id)}</div>);
    }

    return rows;
  },
  renderCells: function(cells, rowId){
    var cellsList = [];

    for (var id in cells){
      cellsList.push(<CellComponent rowId={rowId} cellId={id} cellContain={cells[id]}/>)
    }

    return cellsList;
  },
  render() {
    var field = GameStore.getGame();

    return (
      <div>{this.renderRows(field)}</div>
    );
  }
});

var CellComponent = React.createClass({
  getInitialState:function(){
    return {
      exploded: false,
      open: false
    }
  },
  clickOnCell: function(){
    if (this.props.cellContain === Constants.game.cellStates.mine){

      this.setState({exploded: true});
      alert('Booom!');

      return false;
    }

    ActionCreator.clickOnField(this.props.rowId, this.props.cellId);
    this.setState({open: true});


    //TODO add handele click to cell
  },
  render: function(){

    var classes = cx({
      cell: true,
      exploded: this.state.exploded,
      mine: (this.props.cellContain === Constants.game.cellStates.mine),
      number: true,
      number_1: (this.props.cellContain == 1),
      number_2: (this.props.cellContain == 2),
      number_3: (this.props.cellContain == 3),
      clearCell: (this.props.cellContain == Constants.game.cellStates.openAndNotMIne)
    });

    return (<span className={cx(classes)} onClick={this.clickOnCell}></span>);
  }
});

module.exports = game;
