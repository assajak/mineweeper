var React = require('react');
var  Dispatcher = require('./../dispatchers/AppDispatcher');
var ActionCreator = require('../actions/game');

var header = React.createClass({
  getInitialState() {
    return {
      'mineFlag': "99",
      'time': '00'
    };
  },
  refreshGame: function(){
    ActionCreator.refresh();
  },
  componentWillMount:function(){
    var self = this;

    setInterval(function(){ //TODO move it to store later
        self.setState({
          'time': parseInt(self.state.time, 10)+1
        });
    }, 1000)
  },
  render: function(){
    return (
      <div className='header-container'>
        <span>Score: {this.state.mineFlag}</span>
        <span>
          <button onClick={this.refreshGame}>Refresh</button>
        </span>
        <span>
          {this.state.time}
        </span>
      </div>
    );
  }
});

module.exports = header;
