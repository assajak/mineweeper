var React = require('react');
var HeaderComponent = require('./header.jsx');
var GameComponent = require('./game.jsx');
var GameStore = require('./../stores/Game');



var App = React.createClass({
  componentWillMount: function(){
    GameStore.createFields();
  },
  render() {
    return (
      <div>
        <h1>It's the game</h1>
        <HeaderComponent/>
        <GameComponent/>
      </div>
    );
  }

});

module.exports = App;
