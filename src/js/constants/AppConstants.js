var keyMirror = require('react/lib/keyMirror');

module.exports = {
  game: {
    sizeOfGame: {
      x: 10,
      y: 10
    },
    minesCount: 10,
    cellStates: {
      'mine':'m',
      'void': 0,
      'openAndNotMIne':'v',
      'number': 1 // Number of mines beside
    }
  },

  ActionTypes: keyMirror({
    REFRESH_GAME: 1,
    CHOSE_CELL: 2,
  }),

  ActionSources: keyMirror({
    SERVER_ACTION: null,
    VIEW_ACTION: null
  })

};
